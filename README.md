# ETH Air Drop

Python script for Ether air-drops.

## Getting Started

Set the following environment variables:
```
METAMASK_PVT_KEY
INFURA_API_KEY
```

Update the following variables:
```
network = NetworkConfig.RINKEBY
gas_cost = 60_000
airdrop = 0.01
sender_address = '0x760bf27cd45036a6c486802d30b5d90cffbe31f'
```
